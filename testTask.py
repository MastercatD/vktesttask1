import os
import winreg
import wget


# Поиск папки
def findFolder():
    try:
        key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\Wow6432Node\Valve\Steam")
    except:
        key = winreg.OpenKeyEx(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\Valve\Steam")
    #finally:
        #winreg.CloseKey(winreg.HKEY_LOCAL_MACHINE)
    value = winreg.QueryValueEx(key,"InstallPath")
    return value

# Изменение реестра
def setSettings(keyValue, settings, values):
    key = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, keyValue, 0, winreg.KEY_SET_VALUE)
    i = 0
    for set in settings: 
        winreg.SetValueEx(key, set, 0, winreg.REG_DWORD, values[i])
        i += 1

# Парсинг файла
def parseFile(fileName):
    keyValue = ""
    settings = []
    values = []
    try:
        file = open(fileName, "rb")
        fileText = file.read().decode('utf-16le', 'ignore').split('\n')
        for line in fileText:
            if "SOFTWARE" in line:
                keyValue = line.replace(r"HKEY_CURRENT_USER", "")
            if '=' in line:
                temp = line.split("=")
                settings.append(temp[0].replace('"', ''))
                temp = temp[1].split(':')
                values.append(int(temp[1]))
    except:
        keyValue = r"\Gaggle Studios INC\Goose Goose Duck"


    return keyValue[2:len(keyValue) - 2], settings, values



URL = "https://drive.usercontent.google.com/download?id=1IGENwFzLm8bBEboISadYSNEdxbnjz1fH&confirm=t&uuid=de9a9c05-c16c-4057-9dc2-184daefb83d1&at=APZUnTWTgQBEwlNzTh04toaiZWev%3A1713330871066"

steamPath = findFolder()
gamePath = str(steamPath[0]) +r"\steamapps\common\Goose Goose Duck"
wget.download(URL, gamePath + r"\settings.reg")

keyValue, settings, values = parseFile(gamePath + r"\settings.reg")

setSettings(keyValue, settings, values)

os.startfile(gamePath + r"\Goose Goose Duck.exe")
